<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css" />

    <?php wp_head(); ?>
</head>
<body>
    <?php get_header(); ?>
    <main>
    <div class="image-container">
        <div class="featured-image">
            <?php
            if (has_post_thumbnail() ) {
                $image_url = get_the_post_thumbnail_url();
                echo "<img src=$image_url alt='Featured Image'></img>";
            }
            ?>
        </div>
        <div class="centered">
            <?php
                $excerpt = get_the_excerpt();
                if ($excerpt == null) {
                    echo strtoupper(get_the_title());
                } else {
                    echo $excerpt;
                }
            ?>
        </div>
    </div>
    <div>
        <?php the_content(); ?>
    <div>
    </main>
</body>
</html>


