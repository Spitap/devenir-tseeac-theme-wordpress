<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css" />

    <?php wp_head(); ?>
</head>
<body>
    <?php get_header(); ?>
    <main>
    <div class="image-container">
        <?php
        if (has_post_thumbnail() ) {
            the_post_thumbnail();
        }
        ?>
        <div class="centered">Devenez acteur de la surveillance du secteur aéronautique</div>
    </div>
    </main>


