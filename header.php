<header>
    <div class="flex header">
        <div class="flex site-title">
            <h1><a href=href=<?php get_home_url('/')?>><?php echo strtoupper(get_bloginfo('title')); ?></a></h1>
            <hr class="horizontal-sep" />
        </div>
        <div class="flex">
            <?php echo wp_nav_menu(
            array(
                'theme_location' => 'primary',
                'container' => 'nav',
                'container_class' => 'flex align-center',
                'menu_class' => 'horizontal',
                )
            ); ?>
        </div>
        <div class="flex icon-group">
            <a href="#"><img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/discord.svg" alt="Discord"></img></a>
            <a href="#"><img class="icon" src="<?php echo get_template_directory_uri(); ?>/assets/svg/instagram.svg" alt="Instagram"></img></a>
        </div>
    </div>
    <hr style="padding: 0; margin: 0; width:100vw;"/>
</header>
