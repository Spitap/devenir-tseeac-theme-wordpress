<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="stylesheet" href="<?php echo esc_url( get_stylesheet_uri() ); ?>" type="text/css" />
    <?php wp_head(); ?>
</head>
<body>
<!-- Pour les lecteurs avertis: ALED, sortez-nous de là !!!!!! .-->
    <?php get_header(); ?>
    <main>
    <div class="image-container">
      <div class="featured-image">
        <?php
        if ( has_post_thumbnail() ) {
          the_post_thumbnail();
        }
        ?>
      </div>
        <h1 class="centered"><?php echo strtoupper('Devenez acteur de la surveillance du secteur aÉronautique') ?></h1
        fn>
    </div>
    </main>
</body>
</html>
